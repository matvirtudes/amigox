(function() {
    'use strict';

    angular
        .module('app')
        .service('sessionService', sessionService);

    /** @ngInject */
    function sessionService(sessionRepository, $q, localStorageService, $rootScope) {

        return {
            createSession: createSession,
            updateSession: updateSession,
            deleteSession: deleteSession,
            getAll: getAll
        };

        function getAll(groupId) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            sessionRepository._getAll(groupId, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        function deleteSession(session) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            sessionRepository._deleteSession(session, token)
                .then(function (response) {
                    deferred.resolve(response.data.mensage);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function createSession(session) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            sessionRepository._createSession(session, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function updateSession(session) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            sessionRepository._updateSession(session, token).then(function (response) {
                deferred.resolve(response.data.mensage);
            })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;

        }
       
    }

})();