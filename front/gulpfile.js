var gulp = require("gulp");
var less = require("gulp-less");
var webserver = require("gulp-webserver");

gulp.task("copy", function () {
  gulp.src("./bower_components/bootstrap-sweetalert/dist/**").pipe(gulp.dest("./app/assets/lib/bootstrap-sweetalert"));
  gulp.src("./bower_components/angular-bootstrap-datetimepicker/src/**").pipe(gulp.dest("./app/assets/lib/angular-date-time-input"));
  gulp.src("./bower_components/angular-date-time-input/**").pipe(gulp.dest("./app/assets/lib/angular-bootstrap-datetimepicker"));
  gulp.src("./bower_components/moment/**").pipe(gulp.dest("./app/assets/lib/moment"));
  gulp.src("./bower_components/bootstrap/dist/**").pipe(gulp.dest("./app/assets/lib/bootstrap"));
  gulp.src("./bower_components/angular/**").pipe(gulp.dest("./app/assets/lib/angular"));
  gulp.src("./bower_components/angular-route/**").pipe(gulp.dest("./app/assets/lib/angular"));
  gulp.src("./bower_components/jquery/**").pipe(gulp.dest("./app/assets/lib/jquery"));
  gulp.src("./bower_components/components-font-awesome/**").pipe(gulp.dest("./app/assets/lib/font-awesome"));
});

gulp.task("less", function () {
  gulp.src("./app/assets/less/*.less")
    .pipe(less({ compress: true }))
    .pipe(gulp.dest("./app/assets/less"));
});

gulp.task("watch", function () {
  gulp.watch("./app/assets/less/*.less", ["less"]);
});

gulp.task('webserver', function () {
  gulp.src('app')
    .pipe(webserver({
      livereload: true,
      open: true
    }));
});

gulp.task('deploy', function () {
  var variavel = 'mensagem';

  /**Verifica se a variável foi passada como parametro*/
  if (util.env[variavel] === undefined) {
    util.log(util.colors.red('Não foi definida a variável ' + variavel + ' para ser feito o deploy, Ex: gulp deploy --' + variavel + ' "Mensagem do commit"'));
    return;
  }

  return gulp.src('.')
    .pipe(shell([
      'git add .',
      'git commit -m "' + util.env[variavel] + '"',
      'git push heroku master'
    ]));

});

gulp.task('dist', function () {

  // Carregamos os arquivos novamente
  // E rodamos uma tarefa para concatenação
  // Renomeamos o arquivo que sera minificado e logo depois o minificamos com o `uglify`
  // E pra terminar usamos o `gulp.dest` para colocar os arquivos concatenados e minificados na pasta build/
  gulp.src(files)
    .pipe(concat('./dist'))
    .pipe(rename('dist.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
});

