var app = angular.module('app', ['ngRoute', 'ui.bootstrap.datetimepicker', 'ui.dateTimeInput']);

app.config(function ($routeProvider, $locationProvider) {

    /*$locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });*/

    $routeProvider

        .when('/', {
            templateUrl: '../home/home.html',
            controller: 'HomeCtrl as vm',
            pageKey: 'HOME'
        })       

        .when('/group', {
            templateUrl: '../group/group.html',
            controller: 'GroupCtrl as vm',
        })

        .when('/manage', {
            templateUrl: '../group/manage.html',
            controller: 'GroupCtrl as vm',
        })

        .when('/user', {
            templateUrl: '../user/user.html',
            controller: 'UserCtrl as vm',
        })

        .otherwise({ redirectTo: '/' });
});