<?php
namespace App\Controller;

use App\Controller\AppController;


class ParticipantsController extends AppController
{

   public function initialize()
    	{
        parent::initialize();

        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }             
    }   

}
