(function () {
    'use strict';

    angular
        .module('app')
        .factory('participantRepository', participantRepository);

    participantRepository.$inject = ['$http', '$q'];
    /* @ngInject */
    function participantRepository($http, $q) {
        var path = 'http://localhost:8765/';
        

        return {
            _getAll: getAll,          
            _createParticipant : createParticipant,
            _updateParticipant : updateParticipant,
            _deleteParticipant : deleteParticipant
        };


        function getAll(formData, token){           
            var deferred = $q.defer();          
            $http({
                 url: path + 'users/usersByGroup/' + formData,
                method:'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                }               
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }
        
        function createParticipant(formData, token){            
            var deferred = $q.defer();          
            $http({
                url: path + 'participants',
                method:'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                },
                data: JSON.stringify(formData)
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function updateParticipant(formData, token){
           var deferred = $q.defer();          
            $http({
                url: path + 'participants',
                method:'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                },
                data: JSON.stringify(formData)
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function deleteParticipant(formData, token){
            var deferred = $q.defer();          
            $http({
                url: path + 'participants/' + formData,
                method:'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                }             
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }
    }
})();
