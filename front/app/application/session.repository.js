(function () {
    'use strict';

    angular
        .module('app')
        .factory('sessionRepository', sessionRepository);

    sessionRepository.$inject = ['$http', '$q'];
    /* @ngInject */
    function sessionRepository($http, $q) {
        var path = 'http://localhost:8765/';
        

        return {
            _getAll: getAll,          
            _createSession : createSession,
            _updateSession : updateSession,
            _deleteSession : deleteSession
        };


        function getAll(formData, token){           
            var deferred = $q.defer();          
            $http({
                 url: path + 'sessions/sessionsByGroup/' + formData,
                method:'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                }               
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }
        
        function createSession(formData, token){            
            var deferred = $q.defer();          
            $http({
                url: path + 'sessions',
                method:'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                },
                data: JSON.stringify(formData)
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function updateSession(formData, token){
           var deferred = $q.defer();          
            $http({
                url: path + 'sessions',
                method:'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                },
                data: JSON.stringify(formData)
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function deleteSession(formData, token){
            var deferred = $q.defer();          
            $http({
                url: path + 'sessions/' + formData,
                method:'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                }             
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }
    }
})();
