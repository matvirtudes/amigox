(function () {
    'use strict';

    angular
        .module('app')
        .service('groupService', groupService);

    /** @ngInject */
    function groupService(groupRepository, $q, localStorageService, $rootScope) {

        return {
            createGroup: createGroup,
            updateGroup: updateGroup,
            getAll: getAll,
            get: get
        };

        function getAll(userId) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            groupRepository._getAll(userId, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function get(groupId) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            groupRepository._get(groupId, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function createGroup(group) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            groupRepository._createGroup(group, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;

        }

        function updateGroup(group) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            groupRepository._updateGroup(group, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;

        }

    }

})();