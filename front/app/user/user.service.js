(function() {
    'use strict';

    angular
        .module('app')
        .service('userService', loginService);

    /** @ngInject */
    function loginService(userRepository, $q,localStorageService,$rootScope) {

        return{
            createUser                 : createUser
        };

     
       function createUser(user) {
            
            var token = localStorageService.get('_dataRepository','token');
            var deferred = $q.defer();

            userRepository._createUser(user, token).then(function (response) {  
                deferred.resolve(response.data);
            })
                .catch(function (error) {                   
                    deferred.reject(error);
                });

            return deferred.promise;
        }

    }

})();