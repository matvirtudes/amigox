<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');

Router::defaultRouteClass(DashedRoute::class);

Router::extensions(['json', 'xml']);


Router::scope('/', function (RouteBuilder $routes) {
 $routes->extensions(['json', 'xml']);
    $routes->resources('Users');
    $routes->resources('Groups');
    $routes->resources('Sessions');
    $routes->resources('Participants');       
    $routes->fallbacks('InflectedRoute');
});

Router::connect(
    '/groups/groupsByUser/:id/' ,
    array('controller' => 'groups', 'action' => 'groupsByUser'), 
    array('pass' => array('id'),'id' => '[0-9]+')
 );

 Router::connect(
    '/users/usersByGroup/:id/' ,
    array('controller' => 'users', 'action' => 'usersByGroup'), 
    array('pass' => array('id'),'id' => '[0-9]+')
 );

 Router::connect(
    '/sessions/sessionsByGroup/:id/' ,
    array('controller' => 'sessions', 'action' => 'sessionsByGroup'), 
    array('pass' => array('id'),'id' => '[0-9]+')
 ); 


Plugin::routes();
