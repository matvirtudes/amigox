<?php
namespace App\Controller;

use App\Controller\AppController;


class SessionsController extends AppController
{
     public function initialize()
    {
        parent::initialize();

        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }      
        
    }
    
     public function sessionsByGroup($group_id)
    {         
        
        $sessions = $this->Sessions->find('all', array(
            'conditions'=>array('Sessions.group_id =' => $group_id))
        );

         $this->set([
            'success' => true,
            'data' => $sessions,
            '_serialize' => ['success', 'data']
        ]);
       
    }
    
}
