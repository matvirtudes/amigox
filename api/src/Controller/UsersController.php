<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Validation\Validation;
use Cake\Event\Event;
use Cake\Network\Exception\UnauthorizedException;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UsersController extends AppController
{


    public $paginate = [
        'page' => 1,
        'limit' => 10,
        'maxLimit' => 100,
        'fields' => [
            'id', 'username'
        ],
        'sortWhitelist' => [
            'id', 'username'
        ]
    ];  

    public function initialize()
    {
        parent::initialize();

        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }
        $this->Auth->allow(['add', 'token']);
        
    }

    public function add()
    {
        $this->Crud->on('afterSave', function(Event $event) {
            if ($event->subject->created) {
                $this->set('data', [
                    'id' => $event->subject->entity->id,
                    'token' => JWT::encode(
                        [
                            'sub' => $event->subject->entity->id,
                            'exp' =>  time() + 604800
                        ],
                    Security::salt())
                ]);
                $this->Crud->action()->config('serialize.data', 'data');
            }
        });
        return $this->Crud->execute();
    }

    public function token()
    {    
        $user = $this->Auth->identify();
        
        if (!$user) {
            throw new UnauthorizedException('Email ou senha inválidos');
        }
        $this->set([
            'success' => true,
            'data' => [
                'id' => $user['id'],
                'token' => JWT::encode([
                    'sub' => $user['id'],
                    'exp' =>  time() + 604800
                ],
                Security::salt())
            ],
            '_serialize' => ['success', 'data']
        ]);
    }

     public function usersByGroup($group_id)
    {         
        
        $users = $this->Users->find()->matching('Participants', function ($q) use ($group_id) {
            return $q->where(['Participants.group_id' => $group_id]);
        });

         $this->set([
            'success' => true,
            'data' => $users,
            '_serialize' => ['success', 'data']
        ]);

       
    }
  
}
