/**
 * Created by cristopher on 19/09/2016.
 */
(function () {
    'use strict';

    angular
        .module('app')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', '$q'];
    /* @ngInject */
    function dataservice($http, $q) {

        return {
            _login: login
        };

        function login (loginData) {
            var deferred = $q.defer();           
            $http({
                url: 'http://localhost:8765/users/token',
                method:'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'                 
                },
                data: JSON.stringify(loginData)
            })
            .then(function (data) {
                deferred.resolve(data);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        
    }
})();
