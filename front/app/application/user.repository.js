(function () {
    'use strict';

    angular
        .module('app')
        .factory('userRepository', userRepository);

    userRepository.$inject = ['$http', '$q'];
    /* @ngInject */
    function userRepository($http, $q) {
        var path = 'http://localhost:8765/';
        

        return {                    
            _createUser : createUser,
            _updateUser : updateUser
        };
        
        
        function createUser(formData, token){           
            var deferred = $q.defer();          
            $http({
                url: path + 'users',
                method:'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token         
                },
                data: JSON.stringify(formData)
            })
            .then(function (response) {
                deferred.resolve(response);
            })
            .catch(function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        function updateUser(formData, token){
            var deferred = $q.defer();

            $http.put(path+'users', formData, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined,
                    'Authorization': 'Bearer ' + token
                }
            })
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });
         
            return deferred.promise;
        }
    }
})();
