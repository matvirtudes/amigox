

angular
    .module('app')
    .controller('GroupCtrl', GroupCtrl);

function GroupCtrl(groupService, participantService, sessionService, userService, localStorageService, $location, $filter) {
    var vm = this;
    vm.group = {};

    vm.participantHtml = '../participant/participant.html';
    vm.sessionHtml = '../session/session.html';

    //Group
    vm.save = function (group) {
        if (group.title == null || group.title == undefined) {
            swal("Ocorreu um erro", "O campo nome é obrigatório", "error");
        }
        else {
            groupService.createGroup(group).then(function (response) {
                var userId = localStorageService.get('_dataRepository', 'userId');
                var participant = { 'group_id': response.data.id, 'user_id': userId, 'admin': true }
                participantService.createParticipant(participant)
                    .then(function () {
                        swal('', 'Grupo cadastrado com sucesso', 'success');
                        vm.groups.push(group);
                    })
                    .catch(function (error) {
                        swal('Ocorreu um erro', 'Erro ao cadastrar o grupo', "error")
                    });
            }).catch(function (error) {
                swal('Ocorreu um erro', 'Erro ao cadastrar o grupo', "error")
            });
        }
    };

    vm.update = function (group) {
        console.log(JSON.stringify(group));
        if (group.title == null || group.title == undefined) {
            swal("Ocorreu um erro", "O campo nome é obrigatório", "error");
        }
        else {
            groupService.updateGroup(group)
                .then(function (data) {
                    swal('', 'Grupo atualizado com sucesso', 'success')
                })
                .catch(function (error) {
                    swal('Ocorreu um erro', 'Erro ao atualizar o grupo', "error")
                });
        }
    };

    vm.loadGroups = function () {
        var userId = localStorageService.get('_dataRepository', 'userId');
        groupService.getAll(userId)
            .then(function (response) {
                vm.groups = response.data;
            }).catch(function (error) {
                //swal('Erro ao carregar os grupos', "error")
            });
    };

    vm.loadGroup = function () {
        var groupId = localStorageService.get('_dataRepository', 'groupId');
        groupService.get(groupId)
            .then(function (response) {
                vm.group = response.data;
            }).catch(function (error) {
                //swal('Erro ao carregar os grupos', "error")
            });
    };

    vm.manageGroup = function (group) {
        localStorageService.set('_dataRepository', { groupId: group.id });
        $location.path("/manage");
    };

    //Participants
    vm.user = {};
    vm.loadParticipants = function () {
        var groupId = localStorageService.get('_dataRepository', 'groupId');
        participantService.getAll(groupId)
            .then(function (response) {
                vm.participants = response.data;
            }).catch(function (error) {
                // swal('Erro ao carregar os participantes')
            });
    };

    vm.addParticipant = function (user) {
        if (user.name == null || user.name == undefined) {
            swal("Ocorreu um erro", "O campo nome é obrigatório", "error");
        }
        else if (user.username == null || user.username == undefined) {
            swal("Ocorreu um erro", "O campo email é obrigatório", "error");
        }
        else {
            user.active = true;
            userService.createUser(user).then(function (response) {
                var groupId = localStorageService.get('_dataRepository', 'groupId');
                var participant = { 'group_id': groupId, 'user_id': response.data.id, 'admin': false }
                participantService.createParticipant(participant)
                    .then(function () {
                        swal('', 'Participante cadastrado com sucesso', 'success')
                        vm.participants.push(user);
                    })
                    .catch(function (error) {
                        swal('Ocorreu um erro', 'Erro ao cadastrar o participante', 'error')
                    });
            }).catch(function (error) {
                swal('Ocorreu um erro', 'Erro ao cadastrar o participante', 'error')
            });
        }
    };

    vm.removeParticipant = function (user) {
        participantService.deleteParticipant(user._matchingData.Participants.id)
            .then(function () {
                swal('', 'Participante removido com sucesso', 'success');
                var index = vm.participants.indexOf(session);
                vm.participants.splice(index, 1);
            })
            .catch(function (error) {
                swal('Ocorreu um erro', 'O participante não pode ser removido', 'error');
            });
    };

    //Sessions
    vm.session = {};
    vm.loadSessions = function () {
        var groupId = localStorageService.get('_dataRepository', 'groupId');
        sessionService.getAll(groupId)
            .then(function (response) {
                vm.sessions = response.data;
            }).catch(function (error) {
                //  swal('Erro ao carregar as sessões')
            });
    };

    vm.addSession = function (session) {
        if (session.name == null || session.name == undefined) {
            swal("Ocorreu um erro", "O campo nome é obrigatório", "error");
        } else if (session.place == null || session.place == undefined) {
            swal("Ocorreu um erro", "O campo lugar é obrigatório", "error");
        } else if (session.date == null || session.date == undefined) {
            swal("Ocorreu um erro", "O campo data é obrigatório", "error");
        }
        else {
            session.group_id = localStorageService.get('_dataRepository', 'groupId');
            session.date = $filter('date')(session.date, 'yyyy-MM-dd HH:mm');
            sessionService.createSession(session)
                .then(function (response) {
                    swal('', 'Sessão cadastrada com sucesso', 'success');
                    vm.sessions.push(session);
                }).catch(function (error) {
                    swal("Ocorreu um erro", "Erro ao cadastrar a sessão", "error");
                });
        }
    };

    vm.updateSession = function (session) {
        if (session.title == null || session.title == undefined) {
            swal("Ocorreu um erro", "O campo nome é obrigatório", "error");
        }
        else {
            session.group_id = localStorageService.get('_dataRepository', 'groupId');
            sessionService.updateSession(session)
                .then(function (response) {
                    swal('', 'Sessão atualizada com sucesso', 'success');
                }).catch(function (error) {
                    swal("Ocorreu um erro", "Erro ao atualizar a sessão", "error");
                });
        }
    };


    vm.removeSession = function (session) {
        sessionService.deleteSession(session.id)
            .then(function (response) {
                swal('Sessão removida com sucesso')
                var index = vm.sessions.indexOf(session);
                vm.sessions.splice(index, 1);
            }).catch(function (error) {
                swal('Erro ao remover a sessão')
            });
    };

}
