(function () {
    'use strict';

    angular
        .module('app')
        .service('participantService', participantService);

    /** @ngInject */
    function participantService(participantRepository, $q, localStorageService, $rootScope) {

        return {
            createParticipant: createParticipant,
            updateParticipant: updateParticipant,
            deleteParticipant: deleteParticipant,
            getAll: getAll
        };


        function getAll(groupId) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            participantRepository._getAll(groupId, token)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }


        function deleteParticipant(participant) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            participantRepository._deleteParticipant(participant, token)
                .then(function (response) {
                    deferred.resolve(response.data.mensage);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function createParticipant(participant) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            participantRepository._createParticipant(participant, token)
                .then(function (response) {
                    deferred.resolve(response.data.mensage);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function updateParticipant(participant) {

            var token = localStorageService.get('_dataRepository', 'token');
            var deferred = $q.defer();

            participantRepository._updateParticipant(participant, token).then(function (response) {
                deferred.resolve(response.data.mensage);
            })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;

        }

    }

})();