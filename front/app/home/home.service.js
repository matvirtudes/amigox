(function() {
    'use strict';

    angular
        .module('app')
        .service('loginService', loginService);

    /** @ngInject */
    function loginService(dataservice, $q,localStorageService,$rootScope) {

        return{
            login                 : login,
            logout                : logout,
            setLocalStorage       : setLocalStorage
        };

     
        function setLocalStorage(token){
            localStorageService.set('_dataRepository', {token:token});
        }

        function login(user) {

            var deferred = $q.defer();

            dataservice._login(user).then(function (response) {               
                localStorageService.set('_dataRepository', {token:response.data.data.token});
                localStorageService.set('_dataRepository', {userId:response.data.data.id});
                deferred.resolve(response.data.mensage);

            })
                .catch(function (error) {                   
                    deferred.reject(error);
                });

            return deferred.promise;

        }

        function logout () {
            $rootScope.authentication.isAuth           = false;
            $rootScope.authentication.isAuthEnterprise = false;
            $rootScope.authentication.isPathLogin      = false;
            $rootScope.authentication.isPathStore      = false;
            localStorageService.remove('_dataRepository');
        }

    }

})();