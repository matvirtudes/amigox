/**
 * Created by cristopher on 19/09/2016.
 */
(function() {

    'use strict';

    angular
        .module('app')
        .service('localStorageService', localStorageService);

    /* @ngInject */
    function localStorageService() {

        return {

            set:set,
            get:get,
            remove:remove

        };

        ////////////////

        function set(nameRepository,dataObject) {

            if (localStorage.getItem(nameRepository)!== null) {
                var data = JSON.parse(localStorage.getItem(nameRepository));

                /*jshint -W089 */
                for(var x in dataObject){
                    data[x] = dataObject[x];
                }

                localStorage.setItem(nameRepository, JSON.stringify(data));
                return true;

            }else{
                localStorage.setItem(nameRepository,JSON.stringify(dataObject));
                return true;
            }

        }

        function get (nameRepository,nameKey) {

            if (localStorage.getItem(nameRepository)!== null){
                return JSON.parse(localStorage.getItem(nameRepository))[nameKey];
            }

        }

        function remove (nameRepository) {
            return localStorage.removeItem(nameRepository);
        }

    }

})();