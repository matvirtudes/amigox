

angular
    .module('app')
    .controller('UserCtrl', UserCtrl);

function UserCtrl(userService, $location, $timeout) {
    var vm = this;

    vm.user = {};

    vm.register = function (user) {
        console.log();
        if ((user.name == null || user.name == undefined)
            || (user.password == null || user.password == undefined
                || (user.username == null || user.username == undefined)
            )) {
            swal("Ocorreu um erro", "Todos os campos são obrigatórios", "error");
        }
        else {
            user.active = true;
            userService.createUser(user).then(function () {
                swal("", "Usuário criado com sucesso", "success");
                $timeout(function () {
                    $location.path("/home");
                });
            }).catch(function (error) {
                swal("Ocorreu um erro", error.data.message, "error");
            });
        }
    };

    function createLocalStorage(token) {
        loginService.setLocalStorage(token);
    }
}
