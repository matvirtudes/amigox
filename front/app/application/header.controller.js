angular
    .module('app')
    .controller('HeaderCtrl', HeaderCtrl);

function HeaderCtrl($scope, $location) {

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

    $scope.navColor = "";
    $scope.navClass = function () {
        return $location.path() === "/" ? "navbar-home" : "";
    }
}