<?php
namespace App\Controller;

use App\Controller\AppController;


class GroupsController extends AppController
{
    
    public $paginate = [
        'page' => 1,
        'limit' => 10,
        'maxLimit' => 100,        
        'fields' => [
            'id', 'title'
        ],
        'sortWhitelist' => [
            'id', 'title'
        ]
    ];

	public function initialize()
    	{
        parent::initialize();

        if($this->request->is('options')) {
            $this->response->statusCode(204);
            $this->response->send();
            die();
        }           
    }

    public function groupsByUser($user_id)
    {         
        
        $groups = $this->Groups->find()->matching('Participants', function ($q) use ($user_id) {
            return $q->where(['Participants.user_id' => $user_id]);
        });

         $this->set([
            'success' => true,
            'data' => $groups,
            '_serialize' => ['success', 'data']
        ]);       
    }

   
}
