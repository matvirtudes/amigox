(function () {
    'use strict';

    angular
        .module('app')
        .factory('groupRepository', groupRepository);

    groupRepository.$inject = ['$http', '$q'];
    /* @ngInject */
    function groupRepository($http, $q) {
        var path = 'http://localhost:8765/';


        return {
            _getAll: getAll,
            _get: get,
            _createGroup: createGroup,
            _updateGroup: updateGroup
        };


        function getAll(formData, token) {
            var deferred = $q.defer();
            $http({
                url: path + 'groups/groupsByUser/' + formData,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token
                }
            })
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function get(formData, token) {
            var deferred = $q.defer();
            $http({
                url: path + 'groups/' + formData,
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token
                }
            })
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function createGroup(formData, token) {
            var deferred = $q.defer();
            $http({
                url: path + 'groups',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token
                },
                data: JSON.stringify(formData)
            })
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }

        function updateGroup(formData, token) {
            var deferred = $q.defer();
            $http({
                url: path + 'groups/' + formData.id,
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'bearer ' + token
                },
                data: JSON.stringify(formData)
            })
                .then(function (response) {
                    deferred.resolve(response);
                })
                .catch(function (error) {
                    deferred.reject(error);
                });

            return deferred.promise;
        }
    }
})();
