

angular
    .module('app')
    .controller('HomeCtrl', HomeCtrl);

function HomeCtrl(loginService, $location) {
    var vm = this; 

    vm.user = {};    

    vm.login = function (user) {
        console.log();
        if ((user.username == null || user.username == undefined) || (user.password == null || user.password == undefined)) {
            swal("Ocorreu um erro", "Os campos Email e Senha são obrigatórios", "error");
        }
        else {          

            loginService.login(user).then(function () {
               $location.path( "/group" );
            }).catch(function (error) {
                 swal("Ocorreu um erro", error.data.message, "error");
            });
        }
    };

    function createLocalStorage(token) {
        loginService.setLocalStorage(token);
    }
}
